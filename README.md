# mediabarn

# About
Home server using Fedora CoreOS.

The box is initilzed with Ignition which delivers setup during the inital boot. Ignition includes customizations for networking and host-specific customizations. Finally, Ignition rebases the system to a custom host image.

The custom image to built and then hosted in a container-registry. The image serves to maintain application managment of a fleet of boxes by provising an update channel in which the box will pull its updates from. This approach means the administrator can modify system packages during the lifecycle of the box without needing to "check-in" changes to the boostrapping tool, Ignition, which would require the box to be re-initilzed.

## Ignition vs Image

Ignition manages:
- Host specific customizations
- Static IP
- Custom Hostname
- Any config that has a HARD requirement to be in `/var`

Containerfile manages:
- Cluster-wide host/node settings
- Application configurations that are sensible accross a cluster of boxes
- Universal user configurations that don't need to be in `/var`

# How It Works

1) Pipeline creates Ignition file and Container-Image for `mediabarn`
2) Use Live-ISO to boot into a shell on the Taget bare-metal box
3) After you are on the Live-ISO shell of FedoraCoreOS run the installer commands:

On new FedoraCoreOS, define the branch and URL and verify Ignition. Use a URL shortener first.
```bash
curl -o ./mediabarn.ign http://tinyurl.com/mainmediabarn # main branch
```
Then, install:
```bash
sudo coreos-installer install /dev/sda --ignition-file ./mediabarn.ign
```

Once install has completed, reboot:
```bash
reboot
```

# Optional post-install steps

1) Setup a local admin password: `sudo passwd core`
2) Configure Tailscale creds
3) Configure OpenVPN creds
4) Install flatpaks for waypipe:
```bash
sudo flatpak install flathub \
    com.transmissionbt.Transmission \
    de.haeckerfelix.AudioSharing \
    io.missioncenter.MissionCenter \
    org.mozilla.firefox
```


# Package changes

To modify packages on the box, commit changes to the `mediabarn` Containerfile, and push out changes to `main` branch. This action will update the mediabar Image on the `stable` tag which is what the box consumes.

# Tailscale
To use Tailscale, add a key and start the service. Note, the key will only last 90 days.

1) Get new key - https://login.tailscale.com/admin/settings/keys
2) SSH to box and Add the Key
```bash
sudo sed -i 's/TAILSCALE_AUTH_KEY_CI/NEW_KEY/' /etc/environment
```
3) Then, start the service:
```bash
sudo systemctl start tailscale
```
Note how you do NOT need to "enable" the serivice, as tailscale will automatically attempt to keep the vpn connection online. The service script is just a command to assist with the first-time authentication.
 
## Troubleshoot Tailscale
Check your configured prefs:
```bash
tailscale debug prefs
```

If re-auth is required, these flags may be useful:
```bash
 sudo tailscale up --force-reauth --reset
```

Tailscale links 
- Get new key - https://login.tailscale.com/admin/settings/keys
- About keys - https://tailscale.com/kb/1085/auth-keyss

# OpenVPN

Define your creds:
```bash
sudo su -
cat > /etc/openvpn/pia.txt << EOF
user
pw
EOF
```

Connect openvpn:
```bash
sudo systemctl start pia
sudo systemctl stop pia
```

Check your existing IP before and after with:
```bash
curl -4 icanhazip.com
```

# External storage
Currently, external storage mounts to /var/mnt/data. If you are setting up new external disks, you will need to configure the filestem and folder the first time before connecting it to mediabarn. You will likly want to configure it from your desktop. 


Create folders and files:
```bash
Music
Videos
Plex/Transcode
Plex/Database
Jellyfin/config
Jellyfin/cache
Prometheus/data
Nextcloud/config
```

Remove SELinux confinment:
```bash
sudo chcon -R -t svirt_sandbox_file_t /var/mnt/data
```

# Waypipe examples

Remote forwarding example:
```bash
waypipe -c lz4=9 ssh core@192.168.1.198 flatpak run org.mozilla.firefox
```

# Static IPs and Hostnames

Many common routers will not `request` the Hostname of a box if the box has Static IP configured. Since the services do not require a static IP to operate, this project deploys a box with out a static IP. After the box is online, you should be able to connect over the `hostname`, like `mediabarn.local`.

Most routers will continue to match the same IP to the same box in perpatuity, however, if you expect long periods of downtime for the server the it is possible that the router will allocate the IP to another system. If this is the case, you may want to allocate a Claim IP in the router settings. 


# Manual build

Need to save on CI Minutes?

Buid container and push:
```bash
podman login registry.gitlab.com/barnix/mediabarn/mediabarn -u user -p token

CI_COMMIT_REF_NAME=something

podman build -t registry.gitlab.com/barnix/mediabarn/mediabarn:$CI_COMMIT_REF_NAME -f Containerfile
podman push registry.gitlab.com/barnix/mediabarn/mediabarn:$CI_COMMIT_REF_NAME
```

# Docs
- Butane https://coreos.github.io/butane/config-fcos-v1_5/
- Rebase via Ignition - https://discussion.fedoraproject.org/t/coreos-rebase-on-initilization/86438
- Layering example
    - https://github.com/coreos/layering-examples/blob/main/tailscale/Containerfile
- Docs for native containers https://coreos.github.io/rpm-ostree/container/
- Future of layering https://github.com/coreos/enhancements/blob/main/os/coreos-layering.md
- Fedora Atomic images:
    - https://gitlab.com/fedora/ostree/ci-test
    - https://pagure.io/workstation-ostree-config
